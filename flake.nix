{
  inputs.nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
  inputs.flake-utils.url = "github:numtide/flake-utils";
  inputs.git-gamble.url = "gitlab:pinage404/git-gamble";

  outputs = { self, nixpkgs, flake-utils, git-gamble }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
      in
      {
        devShell = pkgs.mkShell {
          nativeBuildInputs = [
            pkgs.bashInteractive
            pkgs.rustc
            pkgs.cargo
            pkgs.rustfmt
            git-gamble.packages.${system}.git-gamble
          ];
          buildInputs = [ ];
        };
      });
}
