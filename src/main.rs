fn main() {
    println!("Hello, world!");
}

#[derive(PartialEq, PartialOrd)]
enum Diet {
    Vegan,
    Vegetarian,
    Pescetarian,
    Omnivore,
}

#[test]
fn vegan_is_more_accepted_than_vegetarian() {
    assert_eq!(Diet::Vegan < Diet::Vegetarian, true)
}

#[test]
fn vegetarian_is_more_accepted_than_pescetarian() {
    assert_eq!(Diet::Vegetarian < Diet::Pescetarian, true)
}

#[test]
fn pescetarian_is_more_accepted_than_omnivore() {
    assert_eq!(Diet::Pescetarian < Diet::Omnivore, true)
}

#[derive(Debug, PartialEq)]
enum Ingredient {
    Bread,
    Tomato,
    Salad,
    Cheese,
    Fish,
    Ham,
}

trait Food {
    fn respect(&self, diet: &Diet) -> bool;
}

impl Food for Ingredient {
    fn respect(&self, diet: &Diet) -> bool {
        match self {
            Self::Cheese => diet >= &Diet::Vegetarian,
            Self::Fish => diet >= &Diet::Pescetarian,
            Self::Ham => diet >= &Diet::Omnivore,
            _ => true,
        }
    }
}

#[test]
fn bread_is_vegan() {
    assert_eq!(Ingredient::Bread.respect(&Diet::Vegan), true)
}

#[test]
fn cheese_is_not_vegan() {
    assert_eq!(Ingredient::Cheese.respect(&Diet::Vegan), false)
}

#[test]
fn cheese_is_vegetarian() {
    assert_eq!(Ingredient::Cheese.respect(&Diet::Vegetarian), true)
}

#[test]
fn cheese_is_pescetarian() {
    assert_eq!(Ingredient::Cheese.respect(&Diet::Pescetarian), true)
}

#[test]
fn fish_is_not_vegetarian() {
    assert_eq!(Ingredient::Fish.respect(&Diet::Vegetarian), false)
}

#[test]
fn fish_is_pescetarian() {
    assert_eq!(Ingredient::Fish.respect(&Diet::Pescetarian), true)
}

#[test]
fn fish_is_omnivore() {
    assert_eq!(Ingredient::Fish.respect(&Diet::Omnivore), true)
}

#[test]
fn ham_is_not_pescetarian() {
    assert_eq!(Ingredient::Ham.respect(&Diet::Pescetarian), false)
}

#[test]
fn ham_is_omnivore() {
    assert_eq!(Ingredient::Ham.respect(&Diet::Omnivore), true)
}

struct Cooked(Box<dyn Food>, Box<dyn Food>);

impl Food for Cooked {
    fn respect(&self, diet: &Diet) -> bool {
        self.0.respect(diet) && self.1.respect(diet)
    }
}

#[test]
fn diet_must_be_respected_by_all_ingredients() {
    assert_eq!(
        Cooked(Box::new(Ingredient::Bread), Box::new(Ingredient::Ham)).respect(&Diet::Vegan),
        false
    )
}

#[test]
fn diet_must_be_respected_by_all_ingredients_() {
    assert_eq!(
        Cooked(Box::new(Ingredient::Ham), Box::new(Ingredient::Bread)).respect(&Diet::Vegan),
        false
    )
}

#[test]
fn diet_must_be_respected_by_all_cooked_ingredients() {
    assert_eq!(
        Cooked(
            Box::new(Ingredient::Bread),
            Box::new(Cooked(
                Box::new(Ingredient::Ham),
                Box::new(Ingredient::Tomato)
            ))
        )
        .respect(&Diet::Vegan),
        false
    )
}

fn cook(food1: Box<dyn Food>, food2: Box<dyn Food>) -> Cooked {
    Cooked(food1, food2)
}

// doesn't compile because Box doesn't implement PartialEq and Debug
// i don't know how to implements PartialEq and Debug
/*
#[test]
fn no_matter_cooking_order() {
    let panini1 = cook(
        Bow::new(Ingredient::Bread),
        Box::new(cook(
            Bow::new(Ingredient::Tomato),
            Bow::new(Ingredient::Salad),
        )),
    );
    let panini2 = cook(
        Box::new(cook(
            Bow::new(Ingredient::Bread),
            Bow::new(Ingredient::Tomato),
        )),
        Bow::new(Ingredient::Salad),
    );
    assert_eq!(panini1, panini2)
}
*/

#[test]
fn panini_with_at_least_one_non_vegan_ingredient_is_not_vegan() {
    let panini = cook(Box::new(Ingredient::Bread), Box::new(Ingredient::Cheese));
    assert_eq!(panini.respect(&Diet::Vegan), false)
}

#[test]
fn panini_with_at_least_one_non_vegan_ingredient_is_not_vegan_() {
    let panini = cook(Box::new(Ingredient::Bread), Box::new(Ingredient::Ham));
    assert_eq!(panini.respect(&Diet::Vegan), false)
}
